#include <iostream>
#include <fstream>
#include <string.h>

int main(int argc, char** argv) {
    int result = 0;
    if (argc > 3) {
        if (!strcmp(argv[1], "-c")) {
            std::ifstream  src(argv[2], std::ios::binary);
            std::ofstream  dst(argv[3], std::ios::binary);
            dst << src.rdbuf();
        }
        else if (!strcmp(argv[1], "-m")) {
            if (std::rename(argv[2], argv[3])) {
                std::perror("Error renaming");
                return 1;
            }
        }
        else {
            result = -1;
        }
    }
    else {
        result = -1;
    }
    
    if (result) {
        std::cout << "hw_13 [-cm] [input [output]]" << std::endl;
    }
    
    return 0;
}
